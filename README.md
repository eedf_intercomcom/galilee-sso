# galilee-sso
Outils de suivi permettant la migration de Galilée vers un système d'authentification unique (SSO)


## Service de SSO

[Un Keycloak est disponible sur le serveur de longuevue.galilee.edf.fr](https://longuevue.galilee.eedf.fr/keycloak/)


Celui-ci est connecté au LDAP de galilee.

L'idée est de progressivement connecter [tous les services de Galilée](https://galilee.eedf.fr) en OIDC (OpenIDConnect) au Kaycloak qui lui même est lié au LDAP. Les services sont actuellement liés directement au LDAP mais il est nécessaire de se connecter à chaque service individuellement avec des méthodes parfois différentes, alors que l'authentification unique permettra de se connecter, une seule fois et d'une seule manière, pour accéder à tous les services de Galilée.

Suite à l'authentification unique mise en place, on pourra alors envisager de connecter le Keycloak à Jeito une fois qu'il y sera fait quelques ajustements.

## Statut d'avancement de migration SSO des services de Galilée

| Service | Adresse URL | Méthode d'authentification | Connexion OIDC au Keycloak (im/possible, implémentée) | Migration des comptes (possible/implémentée) | Qui? | Pour quand? |
| --- | --- | --- | --- | --- | --- | --- |
| Longuevue | --- | --- | --- | --- | --- | --- |
| Nextcloud | https://galilee.eedf.fr/nextcloud | LDAP | implémentée | réutilisation des mêmes comptes avec une bonne config de l'app oidc | Florence | fait le 06/03/2024 |
| Seafile | https://galilee.eedf.fr/seafile/ | OIDC | possible, implémentée, migrée | fait | Florence | fait le 05/03/2024 |
| Seaforms | https://galilee.eedf.fr/inscriptions/ | via Seafile | renvoie vers Seafile | suit les comptes seafile | Florence | fait le 05/03/2024 |
| Seafile_public_share | https://galilee.eedf.fr/e/ | via Seafile | renvoie vers Seafile | suit les comptes seafile | Florence | fait le 05/03/2024 |
| Mattermost | --- | --- | --- | --- | --- | --- |
| Mail | --- | --- | --- | --- | --- | --- |
| Sympa | --- | --- | --- | --- | --- | --- |
| Camp live | --- | --- | --- | --- | --- | --- |
| Wordpress | --- | --- | --- | --- | --- | --- |
| Moodle | --- | --- | --- | --- | --- | --- |
| Infrastructures mail (postfix, dovecot) | --- | --- | --- | --- | --- | --- |
| Webmail (roundcube) | --- | --- | --- | --- | --- | --- |
| Comptes Unix (pour connexion en ssh) | --- | --- | --- | --- | --- | --- |
| Louflasher2 (newsletter) | --- | --- | --- | --- | --- | --- |
| Kanboard | https://galilee.eedf.fr/kanboard/ | LDAP | proposition de suppression du service | --- | --- | --- |

## Scripts de migrations

### Mattermost 

TODO

### Kanboard

Pas d'utilisation depuis 2022 -> peut-être qu'on peut le supprimer de Galilée ? (surtout que Nextcloud Deck permet de faire la même chose)

### Seafile
 
 Implémenté dans [galilee_gestion_ldap](https://framagit.org/eedf_intercomcom/galilee-ldap) (commande galilee_gestion_ldap seafile_sso -h)
 
 Avant de faire la migration, il faudrait tester la mise à jour vers seafile 
 11.0.4 ou 11.0.5 pour vérifier que tout fonctionne (il y a eu des changements
 sur la page de login dans ces mises à jour).
 
 La version test de seafile : https://longuevue.galilee.eedf.fr/seafile-test/

À faire :

- [x] tester la mise à jour vers Seafile 11.0.5
- [x] vérifier le fonctionnement de la nouvelle page de login
- [x] vérifier le fonctionnement du script de migration dans le cas rencontré récemment de compte LDAP avec juste un mail Galilée
- [x] effectuer la mise à jour vers Seafile 11.0.5 sur Galilée
- [x] mettre à jour la doc de Seafile pour la connexion en OIDC
- [x] lancer la migration : sudo galilee_gestion_ldap seafile_sso --switch-to-oidc
- [x] changer la conf de galilee_ldap pour dire que le changement est fait
- [x] tester seafile_public_share
- [x] tester seafform
- [x] tester synchro ldap/jeito
- [x] tester création de compte
- [x] changer la page d'accueil pour se connecter direct en OIDC (+ changement script post-install)
- [x] supprimer la config LDAP de Seafile si tout va bien.
- [x] infos sur la page d'accueil de galilée
- [x] infos dans Seafile
- [x] info sur la vie de galilée
- [x] info dans galilee.tila.im



### Nextcloud

Fait avec l'app [nextcloud-oidc-login](https://github.com/pulsejet/nextcloud-oidc-login) qui permet un lien facile avec le LDAP (mais besoin d'une config spécifique dans keycloak, pour récupérer les UUID ldap utilisés comme identifiants, et les refiler via OpenIDConnect)

...
